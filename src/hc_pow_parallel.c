/*
-----------------------------------------------------------------------------------------
 Exam project for Cloud e High Performance Computing - Università degli Studi di Palermo
 Developed by: Simone Napoli - 0647275
-----------------------------------------------------------------------------------------

 This program simulates a Hashcash Proof of Work process in a parallel manner.
 It emulates what happens in a cryptocurrency network, based on blockchain.
 
 The problem to be solved is to find a nonce value n such that:
	hash(header+n) be of the form "000000..." 
	(where '+' is a string concatenation)

 The hash function used here is SHA256, like the one used in Bitcoin.

 The approach used is simply to test n from 0 to INT_MAX/2.

 The parallel approach is to divide the problem space among the different process.
 This allow us to execute multiple tries in parallel, without overlaps.

-----------------------------------------------------------------------------------------
*/

#include <stdio.h>

// Usefull to manage strings
#include <string.h>

// Provides the SHA256 implementation (given by openssl)
#include <openssl/sha.h>

// Used just to know how many integer nonce we can try to append to the initial string
#include <limits.h>

// C90 does not support the boolean data type.
#include <stdbool.h>

// MPI implementation
#include "mpi.h"

// Defines how many 0 there have to be at the begin of the searched hash string value
#define LEADINGS_ZERO_N 6

#define FINAL_HEADER_LENGTH 256

#define  MASTER		0




int numtasks, taskid;



/*
  Calculates the sha256 hash function of the given string
  and returns the hash value in a string (char[]).
*/
void calculate_sha256(char* string, char* hashstring)
{
	unsigned char digest[SHA256_DIGEST_LENGTH];
	// calculate hash value
	SHA256((unsigned char*)string, strlen(string), (unsigned char*)&digest);

	// generate hash string (two bytes for one char)
	for(int i = 0; i < SHA256_DIGEST_LENGTH; i++)
		 sprintf(&hashstring[i*2], "%02x", (unsigned int)digest[i]);
}


/*
  Verify if the given hashstring is of the required form.
*/
bool verify_hash(char* hashstring)
{
	for(int i = 0; i < LEADINGS_ZERO_N; i++)
		if(hashstring[i] != '0') return false;

	return true;
}


/*
  Performs the search within the given bounds.
*/
void execute_search(char* header, unsigned int lowerbound, unsigned int upperbound, int taskid, bool stop_at_first)
{
	char header_final[FINAL_HEADER_LENGTH];
	char hashstring[SHA256_DIGEST_LENGTH*2+1];

	// flag for Irecv call
	bool called_recv = false;

	// tag used for results messages
	int tag_for_found = 2;

	// found nonce from tasks (where i is copied)
	int found_nonce;

	// received nonce value from the tasks
	int received_found_nonce;

	// flag for MPI_Test
	int flag;

	/* requests and status for MPI_Isend and MPI_Irecv,
	   used to comunicate results
	*/
	MPI_Request irrequest;
	MPI_Status irstatus;

	MPI_Request isrequest;
	MPI_Status isstatus;

	// starting search
	for(int i = lowerbound; i < upperbound; i++){
		// concatenate the nonce to the header
		sprintf(header_final, "%s:%u", header, i);

		calculate_sha256(header_final, hashstring);

		if(verify_hash(hashstring)){
			
			// found a valid nonce
			found_nonce = i;

			// if master, just print it, if other task, send the result to the master, asynchronously. 
			if(taskid == MASTER){
				printf("FOUND nonce %d by task %d\n", found_nonce, taskid);
				if(stop_at_first) MPI_Abort(MPI_COMM_WORLD, 1);				
			}else{			
				MPI_Isend(&found_nonce, 1, MPI_INT, MASTER, tag_for_found, MPI_COMM_WORLD, &isrequest);
				MPI_Wait(&isrequest, &isstatus);
				printf("sent %d from %d \n", i, taskid);
			}
		}

		/* Master task only, check if there are incoming messages from the tasks */		
		if (taskid == MASTER){
			// call MPI_Irecv if needed and check the flag given by MPI_Test
			if(!called_recv){
				MPI_Irecv(&received_found_nonce, 1, MPI_INT, MPI_ANY_SOURCE, tag_for_found, MPI_COMM_WORLD, &irrequest);
				called_recv = true;
			}else{
				MPI_Test(&irrequest, &flag, &irstatus);
				if(flag != 0){
					printf("FOUND nonce %d by task %d\n", received_found_nonce, irstatus.MPI_SOURCE);
					if(stop_at_first) MPI_Abort(MPI_COMM_WORLD, 1);
					called_recv = false;
					flag = 0;
				}
			}				
		}
	}
}



/*
  MAIN FUNCTION
*/
int main(int argc, char *argv[])
{

	// args validation
	if(argc < 2){
		printf("\n\n Usage:\n\t The first argument must be the header value.\
			   \n\t Pass 's' as second parameter to stop the process when the first solution is found.\
		\n\n");
		return 1;
	}

	unsigned int rangesize = 0, lowerbound = 0, upperbound = 0;
	char* header = argv[1];

	bool stop_at_first = (argc == 3 && argv[2][0] == 's');


	MPI_Status status;
	MPI_Request request;

	// MPI initializations
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	
	MPI_Comm_rank(MPI_COMM_WORLD, &taskid);


	/* Master task only */
	if (taskid == MASTER){

		/*
		  Partitioning dynamically the whole range to all tasks
		  and communicate them their bounds.
		*/

		rangesize = (INT_MAX/2)/numtasks -1;
		upperbound = rangesize;

		printf("[TASK %d %u - %u]\n", taskid, lowerbound, upperbound);

		unsigned int temp_lowerbound = upperbound+1U, temp_upperbound;
	
		for(int i=1; i < numtasks; i++){
	
			MPI_Send(&temp_lowerbound, 1, MPI_UNSIGNED, i, 1, MPI_COMM_WORLD);
			//printf("SENT %u to task %d\n", temp_lowerbound, i);

			temp_upperbound = temp_lowerbound + rangesize;

			MPI_Send(&temp_upperbound, 1, MPI_UNSIGNED, i, 1, MPI_COMM_WORLD);
			//printf("SENT %u to task %d\n", temp_upperbound, i);

			temp_lowerbound = (unsigned int)temp_upperbound+1;
		}

		// master takes its (the first) part
		execute_search(header, lowerbound, upperbound, taskid, stop_at_first);		

	}
	/* Other tasks */
	else {
	
		MPI_Recv(&lowerbound, 1, MPI_UNSIGNED, 0, 1, MPI_COMM_WORLD, &status);
		//printf("RECEIVED %u on task %d\n", lowerbound, taskid);
		MPI_Recv(&upperbound, 1, MPI_UNSIGNED, 0, 1, MPI_COMM_WORLD, &status);
		//printf("RECEIVED %u on task %d\n", upperbound, taskid);
		printf("[TASK %d %u - %u]\n", taskid, lowerbound, upperbound);

		execute_search(header, lowerbound, upperbound, taskid, stop_at_first);

	}

	MPI_Finalize();

}
