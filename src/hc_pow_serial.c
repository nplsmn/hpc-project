/*
-----------------------------------------------------------------------------------------
 Exam project for Cloud e High Performance Computing - Università degli Studi di Palermo
 Developed by: Simone Napoli - 0647275
-----------------------------------------------------------------------------------------

 This program simulates a Hashcash Proof of Work process in a serial manner.
 It emulates what happens in a cryptocurrency network, based on blockchain.
 
 The problem to be solved is to find a nonce value n such that:
	hash(header+n) be of the form "000000..." 
	(where '+' is a string concatenation)

 The hash function used here is SHA256, like the one used in Bitcoin.

 The approach used is simply to test n from 0 to INT_MAX/2.

 The serial approach is to execute hash calculation and test sequentially,
 using just one core and one process. There is no parallelization here.

-----------------------------------------------------------------------------------------
*/


#include <stdio.h>

// Usefull to manage strings
#include <string.h>

// Provides the SHA256 implementation (given by openssl)
#include <openssl/sha.h>

// Used just to know how many integer nonce we can try to append to the initial string
#include <limits.h>

// C90 does not support the boolean data type.
#include <stdbool.h>

// Defines how many 0 there have to be at the begin of the searched hash string value
#define LEADINGS_ZERO_N 6


#define FINAL_HEADER_LENGTH 256

/*
  Calculates the sha256 hash function of the given string
  and returns the hash value in a string (char[]).
*/
void calculate_sha256(char* string, char* hashstring)
{
	unsigned char digest[SHA256_DIGEST_LENGTH];
	// calculate hash value
	SHA256((unsigned char*)string, strlen(string), (unsigned char*)&digest);

	// generate hash string (two bytes for one char)
	for(int i = 0; i < SHA256_DIGEST_LENGTH; i++)
		 sprintf(&hashstring[i*2], "%02x", (unsigned int)digest[i]);
}


/*
  Verify if the given hashstring is of the required form.
*/
bool verify_hash(char* hashstring)
{
	for(int i = 0; i < LEADINGS_ZERO_N; i++)
		if(hashstring[i] != '0') return false;

	return true;
}



/*
  MAIN FUNCTION
*/
int main(int argc, char *argv[])
{

	// args validation
	if(argc < 2){
		printf("\n\n Usage:\n\t The first argument must be the header value.\
			   \n\t Pass 's' as second parameter to stop the process when the first solution is found.\
		\n\n");
		return 1;
	}


	char* header = argv[1];
	char header_final[FINAL_HEADER_LENGTH];
	char hashstring[SHA256_DIGEST_LENGTH*2+1];

	bool stop_at_first = (argc == 3 && argv[2][0] == 's');

	int upperbound = INT_MAX/2;

	// start the searching
	for(int i = 0; i < upperbound; i++){
	// concatenate the nonce to the header
		sprintf(header_final, "%s:%d", header, i);

		calculate_sha256(header_final, hashstring);

		if(verify_hash(hashstring)){
			// found a valid hashstring for the nonce i
			printf("FOUND: %d \t-->\t %s\n", i, hashstring);

			// stop the proces if required
			if(stop_at_first) return 0;
		 }
	}  

	return 0;
}





